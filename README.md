<p align="center">
  <a href="https://kodo.in" target="blank"><img src="https://www.kodo.in/assets/images/kodo-card-website.png" width="320" alt="Kodo Card Img" /></a>
</p>

## Description

Kodo assignment repository.

## Setup and Run tests

```bash
$ yarn setup-and-run-tests
```

You can use the above command to install dependencies, run unit tests, run e2e tests and start the app.

Or you can you use following commands to do those steps individually.

## Installation

```bash
$ yarn
```

## Running the app

The app runs on port `3000`. You can visit graphql playground at `http://localhost:3000/graphql`

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```