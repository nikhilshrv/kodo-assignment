import { SearchService } from '../../lib/search/search.service';

describe('SearchService', () => {

    it('should be able to extract search terms', async () => {
        const searchService = new SearchService({
            searchKeyword: `the king`,
            searchOnKeys: ['name', 'description'],
        });
        expect(searchService.searchTerms).toMatchObject(['the', 'king']);
    });

    it('should be able to get exact search term if the searchKeyword has quotes', async () => {
        const searchService = new SearchService({
            searchKeyword: `'the king'`,
            searchOnKeys: ['name', 'description'],
        });
        expect(searchService.searchTerms).toMatchObject(['the king']);
    });
    
});
