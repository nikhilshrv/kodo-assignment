import { Test } from '@nestjs/testing';
import { PostsRepository } from '@database';
import { GetFeedInput, Sort } from '@contract';

describe('PostsRepository', () => {
    let postsRepository: PostsRepository;
    const samplePosts1 = [
        {
            name: 'A',
            image: 'http://lorempixel.com/640/480',
            description: 'New.',
            dateLastEdited: '2018-05-19T12:33:25.545Z',
        },
        {
            name: 'B',
            image: 'http://lorempixel.com/640/480',
            description: 'Desc B.',
            dateLastEdited: '2017-11-28T04:59:13.759Z',
        },
        {
            name: 'C',
            image: 'http://lorempixel.com/640/480',
            description: 'Desc C.',
            dateLastEdited: '2018-07-27T21:33:53.485Z',
        },
        {
            name: 'D',
            image: 'http://lorempixel.com/640/480',
            description: 'Desc D.',
            dateLastEdited: '2018-07-14T21:01:42.717Z',
        },
        {
            name: 'E',
            image: 'http://lorempixel.com/640/480',
            description: 'Desc E',
            dateLastEdited: '2018-04-18T08:53:42.053Z',
        },
    ];

    const samplePosts2 = [
        {
            name: 'NK',
            image: 'http://lorempixel.com/640/480',
            description: 'Night King',
            dateLastEdited: '2018-05-19T12:33:25.545Z',
        },
        {
            name: 'The Lord of the Rings: The Return of the King',
            image: 'http://lorempixel.com/640/480',
            description: 'LOTR',
            dateLastEdited: '2018-04-18T08:53:42.053Z',
        },
        {
            name: 'The Lion King',
            image: 'http://lorempixel.com/640/480',
            description: 'TLK',
            dateLastEdited: '2018-04-18T08:53:42.053Z',
        },
    ];

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [PostsRepository],
        }).compile();

        postsRepository = moduleRef.get<PostsRepository>(PostsRepository);
    });

    describe('tests for sorting', () => {
        it('should sort by name in asc order', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
            };
            const sortedPosts = postsRepository.sortPosts(input, samplePosts1);
            expect(sortedPosts[0].name).toBe('A');
            expect(sortedPosts[1].name).toBe('B');
        });

        it('should sort by name in desc order', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.desc },
            };
            const sortedPosts = postsRepository.sortPosts(input, samplePosts1);
            expect(sortedPosts[0].name).toBe('E');
            expect(sortedPosts[1].name).toBe('D');
        });

        it('should be able to sort on dateLastEdited', async () => {
            const input: GetFeedInput = {
                page: { size: 1, number: 1 },
                orderBy: { dateLastEdited: Sort.asc },
            };
            const sortedPosts = postsRepository.sortPosts(input, samplePosts1);
            expect(sortedPosts[0].name).toBeTruthy()

            const input2: GetFeedInput = {
                page: { size: 1, number: 1 },
                orderBy: { dateLastEdited: Sort.desc },
            };
            const sortedPosts2 = postsRepository.sortPosts(input2, samplePosts1);
            expect(sortedPosts2[0].name).not.toBe(sortedPosts[0].name);
        });
    });

    describe('tests for pagination', () => {
        it('should be able to get paginated posts', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
            };
            const paginatedPosts = postsRepository.paginatePosts(
                input,
                samplePosts1,
            );
            expect(paginatedPosts[0].name).toBe('A');
            expect(paginatedPosts[1].name).toBe('B');

            const input2: GetFeedInput = {
                page: { size: 2, number: 2 },
                orderBy: { name: Sort.asc },
            };
            const paginatedPosts2 = postsRepository.paginatePosts(
                input2,
                samplePosts1,
            );
            expect(paginatedPosts2[0].name).toBe('C');
            expect(paginatedPosts2[1].name).toBe('D');
        });

        it('should be able to specify the number of posts', async () => {
            const input: GetFeedInput = {
                page: { size: 3, number: 1 },
                orderBy: { name: Sort.asc },
            };
            const sortedPosts = postsRepository.paginatePosts(
                input,
                samplePosts1,
            );
            expect(sortedPosts.length).toBe(3);
            expect(sortedPosts[0].name).toBe('A');
            expect(sortedPosts[1].name).toBe('B');
            expect(sortedPosts[2].name).toBe('C');

            const input2: GetFeedInput = {
                page: { size: 4, number: 1 },
                orderBy: { name: Sort.asc },
            };
            const sortedPosts2 = postsRepository.paginatePosts(
                input2,
                samplePosts1,
            );
            expect(sortedPosts2[0].name).toBe('A');
            expect(sortedPosts2[1].name).toBe('B');
            expect(sortedPosts2[2].name).toBe('C');
            expect(sortedPosts2[3].name).toBe('D');
        });
    });

    describe('tests for search filter', () => {
        it('should be able to get filtered posts based on search term', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
                filter: {
                    searchFilter: 'the king',
                },
            };
            const { posts, total } = postsRepository.filterPosts(
                input,
                samplePosts2,
            );
            expect(posts.length).toBe(2);
            expect(total).toBe(2);
            expect(posts[0].name).toBe(
                'The Lord of the Rings: The Return of the King',
            );
            expect(posts[1].name).toBe('The Lion King');
        });

        it('should be able to get exact matched posts if search term contains phrase within quotes', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
                filter: {
                    searchFilter: `'the king'`,
                },
            };
            const { posts, total } = postsRepository.filterPosts(
                input,
                samplePosts2,
            );
            expect(posts.length).toBe(1);
            expect(total).toBe(1);
            expect(posts[0].name).toBe(
                'The Lord of the Rings: The Return of the King',
            );
        });

        it('should be able to search both name and description', async () => {
            const input: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
                filter: {
                    searchFilter: `NK`,
                },
            };
            const { posts, total } = postsRepository.filterPosts(
                input,
                samplePosts2,
            );
            expect(posts.length).toBe(1);
            expect(total).toBe(1);
            expect(posts[0].name).toBe('NK');
            expect(posts[0].description).toBe('Night King');

            const input2: GetFeedInput = {
                page: { size: 2, number: 1 },
                orderBy: { name: Sort.asc },
                filter: {
                    searchFilter: 'night',
                },
            };
            const { posts: posts2, total: total2 } =
                postsRepository.filterPosts(input2, samplePosts2);
            expect(posts2.length).toBe(1);
            expect(total2).toBe(1);
            expect(posts2[0].name).toBe('NK');
            expect(posts2[0].description).toBe('Night King');
        });
    });
});
