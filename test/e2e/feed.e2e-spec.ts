import { INestApplication } from '@nestjs/common';
import { Sort } from '../../lib/contract';
import * as utils from '../utils';

describe('Posts in feed test', () => {
    let app: INestApplication;

    beforeAll(async () => {
        app = await utils.getINestApp();
    });

    it('should be able to get all posts in the feed', async () => {
        const posts = await utils.feed({
            orderBy: {
                name: Sort.asc,
            },
            page: {
                size: 2,
                number: 1,
            },
        });
        expect(posts.items.length).toBe(2);
        expect(posts.total).toBe(100);
    });

    describe('tests for filter based on search term', () => {
        it('should be filter posts based on search term', async () => {
            const searchTerm = 'senior';
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 2,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(2);
            expect(posts.total).toBe(4);
            expect(
                posts.items.every((item) =>
                    item.name.toLowerCase().includes(searchTerm),
                ),
            ).toBeTruthy();
        });

        it('should be search in description of post', async () => {
            const searchTerm = 'Quis harum rerum similique at';
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 2,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(1);
            expect(posts.total).toBe(1);
            expect(
                posts.items[0].description
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase()),
            ).toBeTruthy();
        });

        it('should be search in name of post', async () => {
            const searchTerm = 'District Solutions Orchestrator';
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 2,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(1);
            expect(posts.total).toBe(1);
            expect(
                posts.items[0].name
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase()),
            ).toBeTruthy();
        });

        it('should be filter exact posts based on search term', async () => {
            const searchTerm = `'the king'`;
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 2,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(2);
            expect(posts.total).toBe(2);
            expect(
                posts.items.every(
                    (item) =>
                        item.name.toLowerCase().includes('the king') ||
                        item.description.toLowerCase().includes('the king'),
                ),
            ).toBeTruthy();
        });
    });

    describe('tests for pagination of posts in feed', () => {
        it('should be get paginated posts in the feed', async () => {
            const searchTerm = 'senior';
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 1,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(1);
            expect(posts.total).toBe(4);

            const posts2 = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 1,
                    number: 2,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts2.items.length).toBe(1);
            expect(posts2.total).toBe(4);
            expect(posts2.items).not.toMatchObject(posts.items);
        });
    });

    describe('tests for sorting the posts', () => {
        it('should be get sorted posts in the feed', async () => {
            const searchTerm = 'senior';
            const posts = await utils.feed({
                orderBy: {
                    name: Sort.asc,
                },
                page: {
                    size: 4,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts.items.length).toBe(4);
            expect(posts.total).toBe(4);

            const posts2 = await utils.feed({
                orderBy: {
                    name: Sort.desc,
                },
                page: {
                    size: 4,
                    number: 1,
                },
                filter: {
                    searchFilter: searchTerm,
                },
            });
            expect(posts2.items.length).toBe(4);
            expect(posts2.total).toBe(4);
            expect(posts2.items[0]).toMatchObject(posts.items[3]);
            expect(posts2.items[1]).toMatchObject(posts.items[2]);
            expect(posts2.items[2]).toMatchObject(posts.items[1]);
            expect(posts2.items[3]).toMatchObject(posts.items[0]);
        });
    });

    afterAll(async () => {
        await app.close();
    });
});
