import { GetFeedInput, Feed } from '@contract';
import * as request from 'supertest';
import { getINestApp } from './utils';

export async function feed(input: GetFeedInput): Promise<Feed> {
    const query = `
        query feed($input: GetFeedInput!){
            feed(input: $input) {
                items {
                    name
                    description
                }
                total
            }
        }
    `;
    const app = await getINestApp();
    const response = await request(app.getHttpServer()).post('/graphql').send({
        query: query,
        variables: { input },
        operationName: 'feed',
    });
    return response.body.data.feed;
}
