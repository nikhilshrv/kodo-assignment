import { Post } from "@contract";

export class SearchService {
    searchOnKeys: string[]
    searchTerms: string[];
    PATTERN_TO_GET_KEYWORDS_BETWEEN_QUOTES = /'((?:[^'\\]|\\\\|\\[^\\])*)'/;

    constructor({
        searchOnKeys,
        searchKeyword,
    }: {
        searchOnKeys: string[],
        searchKeyword: string,
    }) {
        this.searchOnKeys = searchOnKeys;
        this.findSearchTerms(searchKeyword);
    }

    private findSearchTerms(searchKeyword: string) {
        const exactSearchKeyword = searchKeyword.match(this.PATTERN_TO_GET_KEYWORDS_BETWEEN_QUOTES);
        this.searchTerms = exactSearchKeyword?.length ? [exactSearchKeyword[1]] : searchKeyword.split(' ');
    }

    doesPostMatchSearchFilter(post: Post): boolean {
        for (let key of this.searchOnKeys) {
            const doesPostHaveAllSearchTerm = this.searchTerms.every(searchTerm => post[key].toLowerCase().includes(searchTerm.toLowerCase()));
            if (doesPostHaveAllSearchTerm) {
                return true;
            }
        }
    }
}