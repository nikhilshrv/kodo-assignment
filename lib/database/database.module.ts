import { Module } from '@nestjs/common';
import { PostsRepository } from './repositories/posts.repository';

@Module({
    imports: [],
    providers: [PostsRepository],
    exports: [PostsRepository],
})
export class DatabaseModule { }
