import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';
import { Post, Feed, GetFeedInput } from '@contract';
import { mockData } from '../mock_data';
import { SearchService } from '../../search/search.service';

@Injectable()
export class PostsRepository {

    findAll(input: GetFeedInput): Feed {
        let { posts, total } = this.filterPosts(input, mockData);
        posts = this.sortPosts(input, posts);
        posts = this.paginatePosts(input, posts);
        return { items: posts, total };
    }

    filterPosts(input: GetFeedInput, posts: Post[]): { posts: Post[], total: number } {
        if (!input?.filter?.searchFilter) {
            return { posts, total: posts.length };
        }
        const searchService = new SearchService({
            searchOnKeys: ['name', 'description'],
            searchKeyword: input.filter.searchFilter,
        });
        posts = posts.filter(post => searchService.doesPostMatchSearchFilter(post));
        return { posts, total: posts.length };
    }

    sortPosts(input: GetFeedInput, posts: Post[]): Post[] {
        if (!input.orderBy) {
            return posts;
        }
        posts = _(posts).orderBy(Object.keys(input.orderBy), Object.values(input.orderBy)).value();
        return posts;
    }

    paginatePosts(input: GetFeedInput, posts: Post[]): Post[] {
        const limit = input.page.size;
        const skip = (input.page.number - 1) * limit;
        posts = _(posts).drop(skip).take(limit).value();
        return posts;
    }
}
