
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export enum Sort {
    asc = "asc",
    desc = "desc"
}

export interface GetFeedInput {
    filter?: Nullable<GetFeedFilter>;
    page: PageInput;
    orderBy?: Nullable<FeedOrderByInput>;
}

export interface FeedOrderByInput {
    name?: Nullable<Sort>;
    dateLastEdited?: Nullable<Sort>;
}

export interface GetFeedFilter {
    searchFilter?: Nullable<string>;
}

export interface PageInput {
    number: number;
    size: number;
}

export interface Post {
    name: string;
    image: string;
    description: string;
    dateLastEdited: string;
}

export interface Feed {
    items: Nullable<Post>[];
    total: number;
}

export interface IQuery {
    posts(input?: Nullable<GetFeedInput>): Feed | Promise<Feed>;
}

type Nullable<T> = T | null;
