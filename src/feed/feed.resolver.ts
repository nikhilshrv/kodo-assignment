import { Args, Query, Resolver } from '@nestjs/graphql';
import { GetFeedInput, Feed } from '@contract';
import { FeedService } from './feed.service';

@Resolver('Feed')
export class FeedResolver {
    constructor(private readonly postsService: FeedService) {}

    @Query('feed')
    feed(@Args('input') input: GetFeedInput): Feed {
        return this.postsService.findAll(input);
    }
}
