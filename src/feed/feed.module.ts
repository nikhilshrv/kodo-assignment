import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../lib/database';
import { FeedResolver } from './feed.resolver';
import { FeedService } from './feed.service';

@Module({
    imports: [DatabaseModule],
    providers: [FeedService, FeedResolver],
})
export class FeedModule {}
