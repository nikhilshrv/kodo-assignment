import { Injectable } from '@nestjs/common';
import { GetFeedInput, Feed } from '@contract';
import { PostsRepository } from '../../lib/database';

@Injectable()
export class FeedService {
    constructor(private postsRepository: PostsRepository) {}

    findAll(input: GetFeedInput): Feed {
        return this.postsRepository.findAll(input);
    }
}
