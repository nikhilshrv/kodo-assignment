import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { FeedModule } from './feed/feed.module';

@Module({
    imports: [
        FeedModule,
        GraphQLModule.forRoot({
            typePaths: ['./**/*.graphql'],
        }),
    ],
    providers: [],
})
export class AppModule {}
